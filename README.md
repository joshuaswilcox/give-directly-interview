# GiveDirectly Request App

## Getting started
### Setup
Install the dependencies with `bundle install`

### Startup
Start app with `rackup` and visit http://localhost:9292

### Requests
#### Creating a book
    curl -X POST http://localhost:9292/book \
    -H 'Content-Type: application/json' \
    -d '{
	    "title": "My Book Title"
    }'

#### Listing all books
    curl -X GET http://localhost:9292/books

#### Getting one book
    curl -X GET http://localhost:9292/book/1

#### Create request
    curl -X POST http://localhost:9292/request \
    -H 'Content-Type: application/json' \
    -d '{
	    "email": "my@email.com",
        "title": "My Book Title"
    }'

#### Get One Request
    curl -X GET http://localhost:9292/request/1

#### Get all requests
    curl -X GET http://localhost:9292/request

#### Delete Request
    curl -X DELETE http://localhost:9292/request/1