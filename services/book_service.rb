class BookService

    def self.create(title)
        book = Book.new
        book.title = title
        book.available = true
        book.timestamp = DateTime.now.to_s
        book.save
        book
    end

    def self.find(id)
        Book[id]
    end

    def self.all
        Book.dataset
    end

    def self.find_request(id)
        req = BookRequest[id]
        if req
            book = Book[req.book_id]
            {
                "id": req.id,
                "email": req.email,
                "title": book.title
            }
        end
    end

    def self.find_all_requests
        reqs = BookRequest.all

        reqs.map do |r|
            book = Book[r.book_id]
            req = r.values
            {
                "id": req[:id],
                "email": req[:email],
                "title": book.title
            }
        end
    end

    def self.delete_request(id)
        request = BookRequest[id]
        request.delete ? true : false
    end

    def self.reserve(email, title)
        book = Book.where(title: title).first
        return false unless book

        request = BookRequest.new
        request.book_id = book.id
        request.email = email
        if request.save
            book.available = false
            book.save
            {
                "id": request.id,
                "email": email,
                "title": book.title
            }
        else
            nil
        end
    end
end