require_relative '../db/db.rb'

class Book < Sequel::Model(DB.conn[:books])
    Book.plugin :json_serializer
end