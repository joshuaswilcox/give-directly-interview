module Routes
    module Helpers
        def with_valid_email
            request.body.rewind
            data = JSON.parse(request.body.read)
            if data["email"] =~ /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
                yield(data)
            else
                status 403
                return {error: 'Valid email required'}.to_json
            end
        end
    end

    def self.registered(app)
        app.before do
            content_type :json
        end
        app.helpers Helpers
        app.post '/request' do
            with_valid_email do |data|
                new_request = BookService.reserve(data["email"], data["title"])
                if new_request.nil?
                    {"error": "Failed to create request"}.to_json
                else
                    new_request.to_json
                end
            end
        end

        app.get '/request' do
            requests = BookService.find_all_requests

            requests.to_json
        end

        app.get '/request/:id' do
            if params["id"]
                request = BookService.find_request(params["id"])
                request.to_json
            else
                {
                    "error": "Missing id param"
                }.to_json
            end
        end

        app.delete '/request/:id' do
            if params["id"]
                result = BookService.delete_request(params["id"])
                {"success": result}.to_json
            else
                {"error": "Missing id param"}.to_json
            end
        end

        app.post '/book' do
            status 200
            data = JSON.parse(request.body.read)
            book = BookService.create(data["title"])
            book.to_json
        end

        app.get '/books' do
            @books = BookService.all
            @books.to_json
        end

        app.get '/books/:id' do
            status 200
            @book = BookService.find(params[:id])
            if @book
                @book.to_json
            else
                {error: 'Book not found'}.to_json
            end
        end
    end
end