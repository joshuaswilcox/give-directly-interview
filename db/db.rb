module DB
    def conn
        Sequel.sqlite('books.db')
    end

    module_function :conn
end