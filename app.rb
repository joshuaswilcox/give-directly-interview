require 'rubygems'
require 'bundler'
Bundler.require(:default)
Bundler.require(Sinatra::Base.environment)
require 'sinatra/base'
require 'sinatra/json'

module App

    required_directories = %w(
        models
        services
        routes
    )
    require_all required_directories
    require_relative 'db/db.rb'

    class Main < Sinatra::Application
        register Routes
    end
end